package it.unicam.pros.colliery.gui;

public class GUIConstant {
    public static String FRAME_TITLE = "COLLIERY - COLLaboratIve DiscovERY";
    public static String SEPARATOR = " | ";
}
