package it.unicam.pros.colliery.gui;

import it.unicam.pros.colliery.Colliery;
import it.unicam.pros.colliery.core.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Colliery_GUI extends JFrame{


    private JTextArea console;
    private JButton loading;
    private JPanel mainPanel;
    private JButton selectResultLocationButton;
    private JTextField outLocation;
    private JButton discoveryCollaborationButton;
    private JComboBox<DiscoveryAlgorithm> algorithm;
    private JScrollPane scrollPane;
    private JList listaXES;
    private JButton showResults;
    private JButton parametersButton;
    private JButton emptyListButton;
    private String[] xesPaths;
    private boolean xesLoaded;

    public Colliery_GUI(){
        super(GUIConstant.FRAME_TITLE);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setContentPane(mainPanel);
        this.pack();
        this.setSize(800,600);
        initComponents();
    }

    private void initComponents() {
        parametersButton.setEnabled(false);
        showResults.setEnabled(false);
        initConsole();
        for (DiscoveryAlgorithm algo : DiscoveryAlgorithm.values()){
            algorithm.addItem(algo);
        }
        discoveryCollaborationButton.setEnabled(false);
        outLocation.setText(System.getProperty("user.dir")+ File.separator+"collaboration.bpmn");


        loading.addActionListener(e -> {
            loadXesFiles();
        });


        selectResultLocationButton.addActionListener(e -> {
            selectResultPath();
        });
        
        discoveryCollaborationButton.addActionListener(e -> {
            discovery();
        });

        emptyListButton.addActionListener(e -> {
            empyLogPathList();
        });
        
        algorithm.addActionListener(e -> {
            if (algorithm.getSelectedItem().toString().equals(DiscoveryAlgorithm.ALPHA.name()) || algorithm.getSelectedItem().toString().equals(DiscoveryAlgorithm.ALPHA_PLUS.name())){
                parametersButton.setEnabled(false);
            }else{
                parametersButton.setEnabled(true);
            }
        });
        
        parametersButton.addActionListener(e -> {
            if (algorithm.getSelectedItem().toString().equals(DiscoveryAlgorithm.HEURISTICS.name())){
                showHeuristicsParam();
            }
            if (algorithm.getSelectedItem().toString().equals(DiscoveryAlgorithm.INDUCTIVE.name())){
                showInductiveParam();
            }
        });

        showResults.addActionListener(e -> {
            f.setVisible(true);
            f.toFront();
            f.requestFocus();
        });
    }

    private double ind_tres = 0.2;
    private void showInductiveParam() {
        JPanel myPanel = new JPanel();
        JComboBox<InductiveVariants> variant = new JComboBox<>();


        SpinnerNumberModel model = new SpinnerNumberModel(0.2, 0.0, 1.0, 0.01);
        JSpinner noise = new JSpinner(model);
        noise.setValue(ind_tres);
        myPanel.add(new JLabel("Noise threshold"));
        myPanel.add(noise);


        myPanel.setLayout(new BoxLayout(myPanel, BoxLayout.Y_AXIS));
        int result = JOptionPane.showConfirmDialog(
                null, myPanel, "Inductive miner parameters", JOptionPane.OK_OPTION);

        ind_tres = (double) noise.getValue();

    }

    private Map<String, Object> getMiningParams() {
        Map<String, Object> ret = new HashMap<>();
        ret.put("heu_tres", heu_tres);
        ret.put("heu_and_tres", heu_and_tres);
        ret.put("heu_loop_tres", heu_loop_tres);

        ret.put("ind_tres", ind_tres);

        return ret;
    }
    private double heu_tres = 0.5;
    private double heu_and_tres = 0.65;
    private double heu_loop_tres = 1;

    private void showHeuristicsParam() {
        JPanel myPanel = new JPanel();

        SpinnerNumberModel model;

        model = new SpinnerNumberModel(0, 0.0, 1.0, 0.01);
        JSpinner htres = new JSpinner(model);
        htres.setValue(heu_tres);
        myPanel.add(new JLabel("Dependency threshold"));
        myPanel.add(htres);

        model = new SpinnerNumberModel(0.2, 0.0, 1.0, 0.01);
        JSpinner and = new JSpinner(model);
        and.setValue(heu_and_tres);
        myPanel.add(new JLabel("AND measure threshold"));
        myPanel.add(and);

        model = new SpinnerNumberModel(0.2, 0.0, 1.0, 0.01);
        JSpinner loop = new JSpinner(model);
        loop.setValue(heu_loop_tres);
        myPanel.add(new JLabel("Thresholds for the loops of length 2"));
        myPanel.add(loop);


        myPanel.setLayout(new BoxLayout(myPanel, BoxLayout.Y_AXIS));
        int result = JOptionPane.showConfirmDialog(
                null, myPanel, "Heuristics miner parameters", JOptionPane.OK_OPTION);

        heu_tres = (double) htres.getValue();
        heu_and_tres = (double) and.getValue();
        heu_loop_tres = (double) loop.getValue();

    }

    private void discovery() {
        setEnableAll(false);
        //setCorrelationParams();
        CollaborationMiner colliery = new CollaborationMiner();
        Thread minThread = new Thread(colliery);
        colliery.setParams(xesPaths, (DiscoveryAlgorithm) algorithm.getSelectedItem(), getMiningParams());
        minThread.start();
        Thread listener = new Thread(() -> {
            while (!colliery.isTerminated()){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
            try {
                Colliery.logger.log("Serializing BPMN file");
                Colliery.logger.startLoading();
                stringToFile(new File(outLocation.getText()),colliery.getResModel());
                Colliery.logger.stopLoading();
                Colliery.logger.log("Serialization completed at "+outLocation.getText()+"\n");
                setEnableAll(true);
                showResults.setEnabled(true);
                showCommAnResults(colliery.getCommAnalysisResults());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });
        listener.start();
    }

//    private void setCorrelationParams() {
//        JPanel myPanel = new JPanel();
//        for (int i = 0; i < xesPaths.length; i++){
//            List attrs = XESUtils.extractEventAttrs(xesPaths[i]);
////            <string key="msgInstanceId" value="blood sample_255"/>
////			<string key="msgType" value="receive"/>
////			<string key="eventType" value="start"/>
////			<string key="msgFlow" value="blood sample"/>
//            JComboBox<String> msgType = new JComboBox<>(attrs.getItems());
//            msgType.setName("Is send");
//            SpinnerNumberModel model;
//
//            model = new SpinnerNumberModel(0, 0.0, 1.0, 0.01);
//            JSpinner htres = new JSpinner(model);
//            htres.setValue(heu_tres);
//            myPanel.add(new JLabel("Dependency threshold"));
//            myPanel.add(htres);
//
//            model = new SpinnerNumberModel(0.2, 0.0, 1.0, 0.01);
//            JSpinner and = new JSpinner(model);
//            and.setValue(heu_and_tres);
//            myPanel.add(new JLabel("AND measure threshold"));
//            myPanel.add(and);
//
//            model = new SpinnerNumberModel(0.2, 0.0, 1.0, 0.01);
//            JSpinner loop = new JSpinner(model);
//            loop.setValue(heu_loop_tres);
//            myPanel.add(new JLabel("Thresholds for the loops of length 2"));
//            myPanel.add(loop);
//
//
//            myPanel.setLayout(new BoxLayout(myPanel, BoxLayout.Y_AXIS));
//            int result = JOptionPane.showConfirmDialog(
//                    null, myPanel, "Heuristics miner parameters", JOptionPane.OK_OPTION);
//
//            heu_tres = (double) htres.getValue();
//            heu_and_tres = (double) and.getValue();
//            heu_loop_tres = (double) loop.getValue();
//        }
//    }


    private void selectResultPath() {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Select output location");
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Standard BPMN file", "bpmn");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showSaveDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            outLocation.setText(chooser.getSelectedFile().getAbsolutePath());
        }
    }

    private void loadXesFiles() {
        JFileChooser chooser = new JFileChooser(new File("."));
        chooser.setDialogTitle("Select XES files");
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Standard XES files", "xes");
        chooser.setFileFilter(filter);
        chooser.setMultiSelectionEnabled(true);
        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            int len = chooser.getSelectedFiles().length;
            xesPaths = new String[len];
            for (int i = 0; i<len; i++){
                xesPaths[i] = chooser.getSelectedFiles()[i].getAbsolutePath();

            }
            listaXES.setListData(xesPaths);
            discoveryCollaborationButton.setEnabled(true);
        }
    }

    private void empyLogPathList(){
        listaXES.setListData(new String[0]);
        discoveryCollaborationButton.setEnabled(false);
    }

    private void initConsole() {
        console.setColumns(200);
        console.setLineWrap(true);
        console.setWrapStyleWord(false);
        console.setBackground(Color.BLACK);
        console.setEditable(false);
        Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 12);
        console.setFont(font);
        console.setForeground(Color.GREEN);
        console.setAutoscrolls(true);
        TextAreaOutputStream taos = new TextAreaOutputStream( console, 800 );
        PrintStream ps = new PrintStream( taos );
        System.setOut( ps );
        System.setErr( ps );
    }

    private CommAnalysis_GUI f;
    private void showCommAnResults(CommunicationAnalysis commAnalysisResults) {
        f = new CommAnalysis_GUI();

        Set<CommEvent> hang = commAnalysisResults.getHangingMsg();
        Map<String, Set<Long>> del = commAnalysisResults.getMsgDelays();
        Map<String,Integer> msgQnt = commAnalysisResults.getMsgQnt();

        Map<String, Integer> hanAnalysis = new HashMap<>();

        for (CommEvent cEv : hang){
            if (! hanAnalysis.containsKey(cEv.getFlow())) {
                 hanAnalysis.put(cEv.getFlow(), 0);
            }
            hanAnalysis.put(cEv.getFlow(), hanAnalysis.get(cEv.getFlow())+1);
        }
        int[] values = new int[msgQnt.size()];
        int[] qnt = new int[msgQnt.size()];
        String[] names = new String[msgQnt.size()];
        int i = 0;
        for(String flow : msgQnt.keySet()){
            names[i] = flow;
            values[i] = (hanAnalysis.containsKey(flow)) ? hanAnalysis.get(flow) : 0;
            qnt[i]  = msgQnt.get(flow);
            i++;
        }
        f.setHangingPanel(new HangingChart(qnt, values, names));

        double[] minVal = new double[del.size()];
        double[] maxVal = new double[del.size()];
        double[] avgVal = new double[del.size()];
        boolean[] isInf = new boolean[del.size()];
        names = new String[del.size()];
        i = 0;
        for(String flow : del.keySet()){
            names[i] = flow;
            isInf[i] = hanAnalysis.containsKey(flow);
            double min = 0, max = 0, sum = 0; int n = 0;
            for (long l : del.get(flow)){
                if (n == 0) {
                    min = max = l;
                }
                if (l<min) min = l;
                if (l>max) max = l;
                sum += l;
                n++;
            }
            minVal[i] = min/1000;
            maxVal[i] = max/1000;
            avgVal[i] = (sum/n)/1000;
            i++;
        }
        f.setDelaysPanel(new DelaysChart(minVal, maxVal, avgVal, names, isInf));

        f.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        f.setVisible(true);
        f.toFront();
        f.requestFocus();
    }




    private void setEnableAll(boolean b) {
        loading.setEnabled(b);
        selectResultLocationButton.setEnabled(b);
        discoveryCollaborationButton.setEnabled(b);
    }

    private static void stringToFile(File file, String xml) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(xml);
        writer.close();

    }


}
