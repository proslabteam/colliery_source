package it.unicam.pros.colliery.gui;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ConsoleLogger {
    private Thread t;
    private Loading l;
    private static SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    public ConsoleLogger(){
        init();
    }

    public void init(){
        this.l = new Loading();
        this.t = new Thread(l);
    }

    public void startLoading(){
        init();
        t.start();
    }

    public void stopLoading(){
        l.doStop();
    }

    public Thread getLoadingThread(){
        return t;
    }

    public void log(String s){
        System.out.print(formatter.format((new Date()).getTime())+": "+s);
    }


}
