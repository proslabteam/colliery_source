package it.unicam.pros.colliery.gui;

public class Loading implements Runnable {
    private boolean doStop = false;

    public synchronized void doStop() {
        System.out.print("\b");
        System.out.print("\n");
        this.doStop = true;
    }

    private synchronized boolean keepRunning() {
        return this.doStop == false;
    }

    @Override
    public void run() {
        String[] chars = {"|","/","-","\\"};
        int i = 0;
        while (keepRunning()){
            System.out.print(chars[i]);
            i++;
            if(i == 4) i = 0;
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print("\b");
        }
    }
}
