package it.unicam.pros.colliery.gui;

import javax.swing.*;

public class Results extends JFrame{
    private JPanel panel1;
    private JScrollPane delPane;
    private JScrollPane hangPane;

    public Results(){
        super(GUIConstant.FRAME_TITLE+" | Communication analysis results");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setContentPane(panel1);
        this.pack();
        this.setSize(800,600);
        delPane = new JScrollPane();
        hangPane = new JScrollPane();
    }

     public JScrollPane getHangingPanel(){
        return hangPane;
     }

    public JScrollPane getDelayPanel(){
        return hangPane;
    }
}
