package it.unicam.pros.colliery.gui;

import javax.swing.*;
import java.awt.*;

public class DelaysChart extends JPanel {
    private final boolean[] isInfite;
    private double[] minimum;
    private double[] maximus;
    private double[] averages;
    private String[] names;


    public DelaysChart(double[] min, double[] max, double[] avg, String[] names, boolean[] isInf) {
        this.names = names;
        this.averages = avg;
        this.minimum = min;
        this.maximus = max;
        this.isInfite = isInf;
        setSize(800,600);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (names == null || names.length == 0)
            return;
        double minValue = 0;
        double maxValue = 0;
        boolean hasInf = false;
        for (int i = 0; i < names.length; i++) {
            if (minValue > minimum[i])
                minValue = minimum[i];
            if (maxValue < maximus[i])
                maxValue = maximus[i];
            if(isInfite[i]) hasInf = true;
        }
        double infOffSet = maxValue*.3;
        if (hasInf){
            maxValue += infOffSet;
        }

        Dimension d = getSize();
        int clientWidth = d.width;
        int barHeight = 60;

        Font labelFont = new Font("SansSerif", Font.BOLD, 13);
        FontMetrics labelFontMetrics = g.getFontMetrics(labelFont);
        g.setFont(labelFont);
        int bottom = 0;
        int x = 0;

        int valueY = bottom +5, offSet = 10;

        for (int i = 0; i < names.length; i++) {
            if (x< labelFontMetrics.stringWidth(names[i])) x = labelFontMetrics.stringWidth(names[i]);
        }
        x+= labelFontMetrics.stringWidth("Max: ")+15;
        double scale = (clientWidth - 1.5*x ) / (maxValue - minValue);
        for (int i = 0; i < names.length; i++) {
            if (i>0) valueY = i*(barHeight + offSet) + bottom + 5;
            int widthMin = ((int)( minimum[i]*scale));
            int widthAvg = (int) (averages[i] * scale);
            int widthMax =  ((int)( maximus[i]*scale));

            g.setColor(Color.green);
            g.fillRect(x,valueY,widthMin,(barHeight-2)/3);

            g.setColor(Color.yellow);
            g.fillRect(x,valueY+(barHeight-2)/3,widthAvg,(barHeight-2)/3);


            g.setColor(Color.red);
            if (isInfite[i]) {
                g.fillRect(x+widthMax,valueY+2*(barHeight-2)/3, (clientWidth-(widthMax+x)-15),(barHeight-2)/3);
                g.setColor(Color.BLACK);
                g.drawRect(x+widthMax,valueY+2*(barHeight-2)/3,  (clientWidth-(widthMax+x)-15),(barHeight-2)/3);
                g.drawString("Infinite" ,  (clientWidth -70), valueY+ 2*(barHeight-2)/3 +15 );
            }
            g.fillRect(x,valueY+2*(barHeight-2)/3,widthMax,(barHeight-2)/3);


            g.setColor(Color.BLACK);
            g.drawRect(x,valueY,widthMin,(barHeight-2)/3);
            g.drawRect(x,valueY+(barHeight-2)/3,widthAvg,(barHeight-2)/3);
            g.drawRect(x,valueY+2*(barHeight-2)/3,widthMax,(barHeight-2)/3);

            g.drawString(names[i], 5, valueY+2*(barHeight-2)/3);

            int shift = labelFontMetrics.stringWidth("Max: ");
            g.drawString("Min:", x-shift, valueY +15);
            g.drawString("Avg:", x-shift, valueY+ (barHeight-2)/3 +15);
            g.drawString("Max:", x-shift, valueY+ 2*(barHeight-2)/3 +15);

            g.drawString(truncate(minimum[i])+" sec", x+widthMin+10, valueY +15);
            g.drawString(truncate(averages[i])+" sec", x+widthAvg+10, valueY+ (barHeight-2)/3 +15);
            g.drawString(truncate(maximus[i])+" sec", x+widthMax+10, valueY+ 2*(barHeight-2)/3 +15);

        }
    }

    private String truncate(double n) {
        String ret = String.valueOf(n);
        int idx = ret.indexOf(".");
        if (idx == -1) return ret;
        return ret.substring(0,(ret.length()<idx +2) ? ret.length():idx+2);
    }


    public static void main(String[] argv) {
        CommAnalysis_GUI f = new CommAnalysis_GUI();

        double[] mmin = new double[4];
        double[] mmax = new double[4];
        double[] mavg = new double[4];
        boolean[] isInf = new boolean[4];


        String[] names = new String[4];
        mmin[0] = 0;
        mmax[0] = 10;
        mavg[0] = 5.2;
        names[0] = "Item 1";
        isInf[0] = false;

        mmin[1] = 5;
        mmax[1] = 15;
        mavg[1] = 12.2;
        names[1] = "Item 2";
        isInf[1] = false;

        mmin[2] = 10;
        mmax[2] = 1;
        mavg[2] = 1.2;
        names[2] = "Item 3";
        isInf[2] = true;

        mmin[3] = 10;
        mmax[3] = 20;
        mavg[3] = 13.2;
        names[3] = "Item 4";
        isInf[3] = false;

        f.setHangingPanel(new DelaysChart(mmin, mmax,mavg, names, isInf));

        f.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        f.setVisible(true);
    }
}
