package it.unicam.pros.colliery.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class CommAnalysis_GUI extends JFrame {


    private JTabbedPane tabbedPane;
    private JPanel hangingPanel, delaysPanel;

    public CommAnalysis_GUI(){
        super(GUIConstant.FRAME_TITLE+" | Communication analysis results");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        super.setSize(800,600);
        tabbedPane = new JTabbedPane();
        tabbedPane.setSize(800,600);

        hangingPanel = new JPanel();

        delaysPanel = new JPanel();
        hangingPanel.setSize(800,600);
        delaysPanel.setSize(800,600);

        this.getContentPane().add(tabbedPane);
    }

    public JPanel getHangingPanel(){return hangingPanel;}

    public void setHangingPanel(JPanel p){
        tabbedPane.addTab("Hanging messages", null, new JScrollPane(p),
                "Hanging messages");
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
    }

    public void setDelaysPanel(JPanel p){
        tabbedPane.addTab("Delays", null, new JScrollPane(p),
                "Delays");
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
    }
    public JPanel getDelaysPanel(){return delaysPanel;}
}
