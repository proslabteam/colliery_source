package it.unicam.pros.colliery.gui;

import javax.swing.*;
import java.awt.event.ComponentAdapter;
import java.awt.image.BufferedImage;

public class Logo extends JFrame{
    private JPanel mainPanel;
    private JProgressBar progressBar1;
    private JPanel imagePanel;
    private BufferedImage image;
    public Logo(){
        init();
    }

    private void init() {
        this.setSize(600,400);
        mainPanel = new JPanel();
        imagePanel = new JPanel();
        mainPanel.setSize(600,400);
        displayImage(imagePanel, "/splash.jpg");
        this.add(mainPanel);
        this.setResizable(false);
        this.setUndecorated(true);
    }

    private void displayImage(JPanel jp, String url) {
        ImageIcon icon  = new ImageIcon(getClass().getResource(url));

        JLabel jl=new JLabel( icon);
        jl.setSize(600,400);
        jl.setVisible(true);
        jp.add(jl);
    }
}
