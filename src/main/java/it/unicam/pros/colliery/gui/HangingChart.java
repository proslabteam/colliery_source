package it.unicam.pros.colliery.gui;

import javax.swing.*;
import java.awt.*;

public class HangingChart extends JPanel {
    private int[] lost, quantity;

    private String[] names;


    public HangingChart(int[] lost, int[] times, String[] n) {
        this.names = n;
        this.lost = times;
        this.quantity = lost;
        setSize(800,600);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (lost == null || lost.length == 0)
            return;
        double minValue = 0;
        double maxValue = 0;
        for (int i = 0; i < lost.length; i++) {
            if (minValue > lost[i])
                minValue = lost[i];
            if (minValue > quantity[i])
                minValue = quantity[i];
            if (maxValue < lost[i])
                maxValue = lost[i];
            if (maxValue < quantity[i])
                maxValue = quantity[i];
        }


        Dimension d = getSize();
        int clientWidth = d.width;
        int clientHeight = d.height;
        int barWidth = 40;//clientWidth / values.length;


        Font labelFont = new Font("SansSerif", Font.BOLD, 13);
        FontMetrics labelFontMetrics = g.getFontMetrics(labelFont);


        int y =0;
        int x = (clientWidth) / 2;

        int top = 0;
        int bottom = 0;
        if (maxValue == minValue)
            return;


        g.setFont(labelFont);
        int valueY = bottom +5, offSet = 10;

        int namesOffset = 0;
        for (int i = 0; i < names.length; i++) {
            if (namesOffset< labelFontMetrics.stringWidth(names[i]) ) namesOffset = labelFontMetrics.stringWidth(names[i]);
        }
        namesOffset +=  10;
        int typeLabelOffset = labelFontMetrics.stringWidth("Received")+10;

        double scale = (clientWidth  - 1.5*(namesOffset + typeLabelOffset)) / (maxValue - minValue);

        for (int i = 0; i < lost.length; i++) {
            if (i>0) valueY = i*(barWidth + offSet) + bottom + 5;

            int wLost = (int) (lost[i] * scale);
            int wQnt = (int) (quantity[i] * scale);

            g.setColor(gradient(lost[i], quantity[i]));
            g.fillRect(namesOffset + typeLabelOffset, valueY, wLost,(barWidth - 2)/2);
            g.setColor(new Color(0, 102, 255));
            g.fillRect(namesOffset + typeLabelOffset, valueY+(barWidth - 2)/2, wQnt,(barWidth - 2)/2);

            g.setColor(Color.black);
            g.drawRect(namesOffset + typeLabelOffset, valueY+(barWidth - 2)/2, wQnt,(barWidth - 2)/2);
            g.drawRect(namesOffset + typeLabelOffset, valueY, wLost,(barWidth - 2)/2);

            y = valueY+15;
            g.drawString(names[i], 5, y);
            g.drawString("Lost", namesOffset + 5, y);
            g.drawString("Received", namesOffset + 5, y +(barWidth - 2)/2);
            x = namesOffset + typeLabelOffset + wLost ;
            g.drawString(String.valueOf(lost[i]), x+5, y);
            x = namesOffset + typeLabelOffset + wQnt ;
            g.drawString(String.valueOf(quantity[i]), x+5, y+(barWidth - 2)/2);
        }
    }

    private Color gradient(double value, double maxValue) {
        double p = value/maxValue;
        if (p<.2) return new Color(105, 179, 76);
        if (p>=.2 && p<.4) return new Color(172, 179, 52);
        if (p>=.4 && p<.6) return new Color(250, 183, 51);
        if (p>=.6 && p<.8) return new Color(255, 142, 21);
        if (p>=.8 ) return new Color(255, 13, 13);
        return  null;
    }

    public static void main(String[] argv) {
        CommAnalysis_GUI f = new CommAnalysis_GUI();

        int[] times = new int[3];
        int[] lost = new int[3];
        String[] names = new String[3];
        times[0] = 100;
        lost[0] = 2;
        names[0] = "Item 1";

        times[1] = 2;
        lost[1] = 1;
        names[1] = "Item 2";

        times[2] = 400;
        lost[2] = 200;
        names[2] = "Item 3";

        f.setHangingPanel(new HangingChart(times, lost, names));

        f.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        f.setVisible(true);
    }
}
