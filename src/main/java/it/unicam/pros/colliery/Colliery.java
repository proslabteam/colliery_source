package it.unicam.pros.colliery;
import it.unicam.pros.colliery.core.Pm4PyBridge;
import it.unicam.pros.colliery.gui.Colliery_GUI;
import it.unicam.pros.colliery.gui.ConsoleLogger;

import javax.script.ScriptException;
import javax.swing.*;
import java.awt.*;

public class Colliery {

    public static ConsoleLogger logger = new ConsoleLogger();

    public static void main(String[] args){

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        JFrame frame =  new Colliery_GUI();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        //Check script presence
        try {
            Pm4PyBridge.checkScripts();
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        //---------------------------


    }

}
