package it.unicam.pros.colliery.core;

import it.unicam.pros.colliery.Colliery;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.StartEvent;

import javax.script.ScriptException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.List;

public class CollaborationMiner implements Runnable{

    private String[] paths; DiscoveryAlgorithm a;
    private String resModel = null;
    private boolean isTerminated = false;
    private static CommunicationAnalysis results;
    private static Map<String, Object> miningParams;
    public boolean isTerminated(){ return isTerminated;}
    public String getResModel(){
        return resModel;
    }
    public CommunicationAnalysis getCommAnalysisResults(){
        return results;
    }

    public void setParams(String[] pathsToXESFiles, DiscoveryAlgorithm algo, Map<String, Object> params){
        resModel = "";
        paths = pathsToXESFiles;
        a = algo;
        isTerminated = false;
        setMiningParams(params);
    }


    private void setMiningParams(Map<String, Object> params) {
        miningParams = params;
    }

    @Override
    public void run() {
        try {
            resModel = mineLogs(paths, a);
            isTerminated = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static String mineLogs(String[] pathsToXESFiles, DiscoveryAlgorithm algo) throws Exception {
        List<String> xmlModels = new ArrayList<>(pathsToXESFiles.length);
        Communication communications = new Communication();
        for(int i  = 0; i < pathsToXESFiles.length; i++){
            BpmnModelInstance process = null;
            Colliery.logger.log("Discovery on "+pathsToXESFiles[i]+" with "+algo+"...");
            Colliery.logger.startLoading();
            process = discovery(pathsToXESFiles[i], algo);
            Colliery.logger.stopLoading();
            Colliery.logger.log("Discovery on "+pathsToXESFiles[i]+" completed.\n");
            Colliery.logger.log("Extract communication from "+pathsToXESFiles[i]+"...");
            Colliery.logger.startLoading();
            communications.addAll(i, XESUtils.extractCommunication(i, pathsToXESFiles[i]));
            Colliery.logger.stopLoading();
            Colliery.logger.log("Extraction from"+pathsToXESFiles[i]+" completed.\n");
            Colliery.logger.log("Convert communication events for "+pathsToXESFiles[i]+"...");
            Colliery.logger.startLoading();
            String proc = Bpmn.convertToString(process);
            proc = convertCommunicationEvents(proc, communications, i);
            Colliery.logger.stopLoading();
            Colliery.logger.log("Convertion on "+pathsToXESFiles[i]+" completed.\n");
            Colliery.logger.log("Apply model fixes on "+pathsToXESFiles[i]+"...");
            Colliery.logger.startLoading();
            proc = BPMNUtils.convertXorGateways(proc);
            Colliery.logger.stopLoading();
            Colliery.logger.log("Model fixing "+pathsToXESFiles[i]+" completed.\n");
            xmlModels.add(proc);
        }
        Colliery.logger.log("Group processes...");
        Colliery.logger.startLoading();
        String finalCollaboration = BPMNUtils.groupProcesses(xmlModels);
        Colliery.logger.stopLoading();
        Colliery.logger.log("Processes correctly grouped.\n");

        Colliery.logger.log("Analyse communication...");
        Colliery.logger.startLoading();
        results = new CommunicationAnalysis(communications);
        Colliery.logger.stopLoading();
        Colliery.logger.log("Analysis completed.\n");

        Colliery.logger.log("Decorate collaboration with communication aspects...");
        Colliery.logger.startLoading();
        finalCollaboration = applyCommunication(finalCollaboration, communications);
        finalCollaboration = BPMNUtils.convertStartEndMessageEvents(finalCollaboration);
        Colliery.logger.stopLoading();
        Colliery.logger.log("Decoration completed.\n");
        return finalCollaboration;
    }


    private static String convertCommunicationEvents(String process, Communication communications, int pid){
        Map<Integer, Map<String, MsgType>> msgType = communications.getMsgTypes();
        Map<String, CommType> commType = communications.getCommTypes();
        for (String eventID : msgType.get(pid).keySet()) {
            if (commType.get(eventID) == CommType.P2P)
                process = BPMNUtils.makeMsgTask(process, eventID, msgType.get(pid).get(eventID));
            else if (commType.get(eventID) == CommType.BROADCAST)
                process = BPMNUtils.makeSignal(process, eventID, msgType.get(pid).get(eventID), pid);
        }
        return process;
    }

    private static String applyCommunication(String finalCollaboration, Communication communications) {
        Map<String, CommType> commType = communications.getCommTypes();
        Map<String, MsgConnections> msgFlows = communications.getMsgFlows();
        Map<String, String> events = communications.getMsgEvents();

        for (String msg : msgFlows.keySet()){
            if (commType.get(msgFlows.get(msg).getSource()) == CommType.P2P)
                finalCollaboration = BPMNUtils.makeMsgFlow(finalCollaboration, msg, msgFlows.get(msg).getSource(), msgFlows.get(msg).getTarget());
        }
//        for (String ev : events.keySet()){
//            if (commType.get(ev) == CommType.P2P)
//                finalCollaboration = BPMNUtils.convertMsgEvents(finalCollaboration, ev, events.get(ev));
//        }
//        finalCollaboration = BPMNUtils.convertXorGateways(finalCollaboration);
        return finalCollaboration;
    }


    private static BpmnModelInstance discovery(String filePath, DiscoveryAlgorithm algo) throws Exception {
        String rawProcess = AlgorithmsBridge.discovery(filePath, algo, miningParams);
        String processXML  = BPMNUtils.fixRawProcess(rawProcess);
        BpmnModelInstance collaboration = BPMNUtils.generateCollaborationInstance(processXML, filePath);
        return collaboration;
    }


}

