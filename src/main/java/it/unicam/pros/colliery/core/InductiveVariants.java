package it.unicam.pros.colliery.core;

public enum InductiveVariants {
    IM, IMf, IMd
}
