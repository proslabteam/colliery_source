package it.unicam.pros.colliery.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommEvent{
    private String activity, msgInstanceId, flow;
    private MsgType type;
    private Date date;
    private String isMsgEvent, time;
    private CommType commType;
    private int pid;

    public CommType getCommType() {
        return commType;
    }

    public void setCommType(CommType commType) {
        this.commType = commType;
    }

    public CommEvent(int pid, String activity, MsgType type, String msgInstanceId, String mFlow, String mTime, String isMsgEvent, CommType commType){
        this.pid = pid;
        this.type = type;
        this.activity = activity;
        this.msgInstanceId = msgInstanceId;
        this.flow = mFlow;
        this.isMsgEvent = isMsgEvent;
        this.commType = commType;
        this.time = mTime;
        mTime = mTime.substring(0,mTime.indexOf("+")).replace("T", " ");
        try {
            this.date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(mTime);
        } catch (ParseException e) {
            this.date = new Date();
        }
    }

    public int getPid() {return  this.pid;}
    public String getTime() {return this.time;}
    public Date getDate() {return this.date;}

    public String isMsgEvent() {
        return this.isMsgEvent;
    }

    public String getInstanceId() {
        return msgInstanceId;
    }

    public void setInstanceId(String content) {
        this.msgInstanceId = content;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public MsgType getType() {
        return type;
    }

    public void setType(MsgType type) {
        this.type = type;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}
