package it.unicam.pros.colliery.core;

public enum DiscoveryAlgorithm {
    ALPHA, ALPHA_PLUS, HEURISTICS, INDUCTIVE, SPLIT_MINER
}
